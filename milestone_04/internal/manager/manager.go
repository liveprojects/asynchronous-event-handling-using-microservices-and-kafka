package manager

import (
	"encoding/json"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"inventory/internal/events"
	"os"
	"sync"
)

type Manager struct {

	orderConsumer			*kafka.Consumer

	orderConfirmedProducer	*kafka.Producer

	orderFailedProducer		*kafka.Producer
}

func NewManager(broker string) (*Manager, error) {

	var o *kafka.Consumer
	var oc, of *kafka.Producer
	var e error

	o,e = createConsumer(broker, "received_orders")
	if e!=nil {
		return nil, e
	}

	oc, e = createProducer(broker)
	if e!= nil {
		return nil, e
	}

	of, e = createProducer(broker)
	if e!= nil {
		return nil, e
	}


	m := Manager{
		orderConsumer:          o,
		orderConfirmedProducer: oc,
		orderFailedProducer:    of,
	}

	return &m, nil
}


func createProducer(broker string) (*kafka.Producer, error) {

	hn, _ := os.Hostname()

	cCfg := kafka.ConfigMap{
		"broker.address.family": "v4",
		"bootstrap.servers": broker,
		"client.id":         hn}

	p, err := kafka.NewProducer(&cCfg)

	if err != nil {
		fmt.Println(fmt.Sprintf("Failed to create producer: %s\n", err))
		return nil, err
	}

	return p, nil
}


func createConsumer(broker string, topic string) (*kafka.Consumer, error) {

	cCfg := kafka.ConfigMap{
		"broker.address.family": "v4",
		"bootstrap.servers": broker,
		"session.timeout.ms":    6000,
		"group.id": "inventory",
		"auto.offset.reset":     "earliest"}

	c, err := kafka.NewConsumer(&cCfg)

	if err != nil {
		fmt.Println(fmt.Sprintf("Failed to create consumer: %s\n", err))
		return nil, err
	}

	c.Subscribe(topic, nil)

	return c, nil
}

func(m Manager) Subscribe(topic string, ch *chan events.OrderReceived, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		ev := m.orderConsumer.Poll(100)
		if ev == nil {
			continue
		}

		switch e := ev.(type) {
		case *kafka.Message:
			fmt.Printf("%% Message on %s:\n%s\n",
				e.TopicPartition, string(e.Value))
			if e.Headers != nil {
				fmt.Printf("%% Headers: %v\n", e.Headers)
			}

			or := events.OrderReceived{}

			json.Unmarshal(e.Value, &or)

			*ch <- or

		case kafka.Error:
			// Errors should generally be considered
			// informational, the client will try to
			// automatically recover.
			// But in this example we choose to terminate
			// the application if all brokers are down.
			fmt.Fprintf(os.Stderr, "%% Error: %v: %v\n", e.Code(), e)

			/*
			if e.Code() == kafka.ErrAllBrokersDown {
				run = false
			}
			*/

		default:
			fmt.Printf("Ignored %v\n", e)
		}
	}
}


func (m Manager) ConfirmOrderProcess(topic string, ch *chan(events.OrderReceived), wg *sync.WaitGroup) error {

	defer wg.Done()

	for {
		or := <-*ch

		oc := events.OrderConfirmed{
			ID:         or.CustomerID,
			ProductID:  or.ProductID,
			ProdDesc:   or.ProdDesc,
			CustomerID: or.CustomerID,
			Time:       or.Time,
			Confirmed:  true,
		}

		blob, e := json.Marshal(oc)

		if e != nil {
			return e
		}

		msg := kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
			Value:          blob}

		deliveryChan := make(chan kafka.Event, 10)
		m.orderConfirmedProducer.Produce(&msg, deliveryChan)

		err := <-deliveryChan
		km := err.(*kafka.Message)

		if km.TopicPartition.Error != nil {
			fmt.Printf("Delivery failed: %v\n", km.TopicPartition.Error)
		} else {
			fmt.Printf("Delivered message to topic %s [%d] at offset %v\n",
				*km.TopicPartition.Topic, km.TopicPartition.Partition, km.TopicPartition.Offset)
		}

		close(deliveryChan)
	}
	return nil
}