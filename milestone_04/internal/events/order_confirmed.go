package events

import (
	"encoding/json"
	"time"
)

type OrderConfirmed struct {
	ID         uint64    `json:"id"`
	ProductID  uint32    `json:"prod_id"`
	ProdDesc   string    `json:"prod_desc"`
	CustomerID uint64    `json:"customer_id"`
	Time       time.Time `json:"time"`
	Confirmed  bool		 `json:"confirmed"`
}


func NewOrderConfirmed(msg []byte) (*OrderConfirmed, error) {

	var order OrderConfirmed

	err := json.Unmarshal(msg, &order)

	if err != nil {
		return nil, err
	}

	order.Confirmed=true

	return &order, nil
}