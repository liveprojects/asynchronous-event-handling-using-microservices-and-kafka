package events

import (
	"time"
)

type FailedOrder struct {
	ID         	uint64    `json:"id"`
	Time       	time.Time `json:"time"`
	ErrorCode  	uint64		`json:"error_code"`
	ErrorStr	string		`json:"error"`

}
