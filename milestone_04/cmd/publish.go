package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"inventory/internal/events"
	"os"
)

func PublishConfirmedOrder(order events.OrderConfirmed) error {

	hn, _ := os.Hostname()

	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("broker"),
		"client.id":         hn,
		"acks":              "all"})

	if err != nil {
		fmt.Println(fmt.Sprintf("Failed to create producer: %s\n", err))
		return err
	}

	topic := viper.GetString("topic")

	blob, err := json.Marshal(order)

	if err != nil {
		return err
	}

	deliveryChan := make(chan kafka.Event, 10000)

	msg := kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          blob}

	err = p.Produce(&msg,
		deliveryChan,
	)

	e := <-deliveryChan
	m := e.(*kafka.Message)

	if m.TopicPartition.Error != nil {
		fmt.Printf("Delivery failed: %v\n", m.TopicPartition.Error)
	} else {
		fmt.Printf("Delivered message to topic %s [%d] at offset %v\n",
			*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset)
	}

	close(deliveryChan)

	return nil

}
