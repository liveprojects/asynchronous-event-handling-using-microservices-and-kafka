package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"inventory/internal/events"
	"os"
	"os/signal"
	"syscall"
)

func Subscribe(topic string) error {
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	//hn, _ := os.Hostname()
	fmt.Println(viper.GetString("broker"))
	cCfg := kafka.ConfigMap{
		"broker.address.family": "v4",
		"bootstrap.servers": viper.GetString("broker"),
		//"client.id":         hn,
		"session.timeout.ms":    6000,
		"group.id": "inventory",
		"auto.offset.reset":     "earliest"}

	c, err := kafka.NewConsumer(&cCfg)

	if err != nil {
		fmt.Println(fmt.Sprintf("Failed to create producer: %s\n", err))
		return err
	}

	c.Subscribe(topic, nil)

	run := true

	for run == true {
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			ev := c.Poll(100)
			if ev == nil {
				continue
			}

			switch e := ev.(type) {
			case *kafka.Message:
				fmt.Printf("%% Message on %s:\n%s\n",
					e.TopicPartition, string(e.Value))
				if e.Headers != nil {
					fmt.Printf("%% Headers: %v\n", e.Headers)
				}


				processMsg(e.Value)

			case kafka.Error:
				// Errors should generally be considered
				// informational, the client will try to
				// automatically recover.
				// But in this example we choose to terminate
				// the application if all brokers are down.
				fmt.Fprintf(os.Stderr, "%% Error: %v: %v\n", e.Code(), e)
				if e.Code() == kafka.ErrAllBrokersDown {
					run = false
				}
			default:
				fmt.Printf("Ignored %v\n", e)
			}
		}
	}
	return nil
}


func processMsg(msg []byte) error {

	o := events.OrderReceived{}

	e := json.Unmarshal(msg, &o)

	if e!=nil {
		return e
	}

	return nil
}