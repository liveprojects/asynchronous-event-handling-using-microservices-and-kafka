package cmd

import (
	"inventory/internal/events"
	"inventory/internal/manager"
	"sync"
)

func DoWork()  {

	mgr, e := manager.NewManager("192.168.2.30:9092")

	if e != nil {
		return
	}

	ch := make(chan events.OrderReceived)
	wg := sync.WaitGroup{}

	wg.Add(1)
	go mgr.Subscribe("order_received", &ch , &wg)

	wg.Add(1)
	go mgr.ConfirmOrderProcess("order_confirmed", &ch, &wg)

	wg.Wait()
}
