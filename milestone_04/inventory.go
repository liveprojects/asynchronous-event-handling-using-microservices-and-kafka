package main

import (
	//log "github.com/sirupsen/logrus"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
	"inventory/cmd"
)



func main() {

	flag.String("broker", "127.0.0.1:9092", "Kafka broker endpoint")
	flag.String("topic", "received_orders", "Kafka topic where the orders will be published")
	flag.String("error", "wrong_orders", "Kafka topic where the orders will be published")
	viper.BindPFlags(flag.CommandLine)

	flag.Parse()

	cmd.DoWork()
	//cmd.Subscribe(viper.GetString("topic"))

}