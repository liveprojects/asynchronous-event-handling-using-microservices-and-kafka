## Milestone 1

Content:

* [Install.md](./Install.md) : describes `zookeeper` and `kafka` installation steps

* [conf](./conf) : contains some notes about `zookeeper` and `kafka` configuration

* [events](./events) : contains events description

* [src](./src) : contains simple script to start and stop `zookeeper` and `kafka`

* [systemd](./systemd) : contains `systemd` setting for `zookeeper` and `kafka`