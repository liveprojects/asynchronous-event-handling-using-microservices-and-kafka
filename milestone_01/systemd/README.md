
As `root` user, copy files into `/etc/systemd/system` folder

## Zookeeper

### start

```shell
# systemctl start zookeeper.service

# systemctl status zookeeper.service
● zookeeper.service - ZooKeeper Service
   Loaded: loaded (/etc/systemd/system/zookeeper.service; disabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-02-15 16:20:19 GMT; 3s ago
     Docs: http://zookeeper.apache.org
  Process: 5947 ExecStart=/opt/zookeeper/3.6.2/bin/zkServer.sh start /opt/zookeeper/3.6.2/conf/zoo.cfg (code=exited, status=0/SUCCESS)
 Main PID: 5962 (java)
    Tasks: 36 (limit: 4701)
   Memory: 53.0M
   CGroup: /system.slice/zookeeper.service
           └─5962 java -Dzookeeper.log.dir=/opt/zookeeper/3.6.2/bin/../logs -Dzookeeper.log.file=zookeeper-zookeeper-server-debian.log -Dzookeeper.root.logger=INFO,CO

Feb 15 16:20:18 debian systemd[1]: Starting ZooKeeper Service...
Feb 15 16:20:18 debian zkServer.sh[5947]: /usr/bin/java
Feb 15 16:20:18 debian zkServer.sh[5947]: ZooKeeper JMX enabled by default
Feb 15 16:20:18 debian zkServer.sh[5947]: Using config: /opt/zookeeper/3.6.2/conf/zoo.cfg
Feb 15 16:20:19 debian zkServer.sh[5947]: Starting zookeeper ... STARTED
Feb 15 16:20:19 debian systemd[1]: Started ZooKeeper Service.
```

### stop

```shell
# systemctl stop zookeeper.service
```


# Kafka

### start

As `root` user

```shell
# systemctl start kafka.service

# systemctl status kafka.service
● kafka.service - Apache Kafka server (broker)
   Loaded: loaded (/etc/systemd/system/kafka.service; disabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-02-15 16:21:37 GMT; 4s ago
     Docs: http://kafka.apache.org/documentation.html
 Main PID: 6006 (java)
    Tasks: 69 (limit: 4701)
   Memory: 315.4M
   CGroup: /system.slice/kafka.service
           └─6006 java -Xmx1G -Xms1G -server -XX:+UseG1GC -XX:MaxGCPauseMillis=20 -XX:InitiatingHeapOccupancyPercent=35 -XX:+ExplicitGCInvokesConcurrent -XX:MaxInline

Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,457] INFO [ExpirationReaper-0-AlterAcls]: Starting (kafka.server.DelayedOperationPurgatory$Ex
Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,513] INFO [/config/changes-event-process-thread]: Starting (kafka.common.ZkNodeChangeNotifica
Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,529] INFO [SocketServer brokerId=0] Starting socket server acceptors and processors (kafka.ne
Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,533] INFO [SocketServer brokerId=0] Started data-plane acceptor and processor(s) for endpoint
Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,533] INFO [SocketServer brokerId=0] Started socket server acceptors and processors (kafka.net
Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,553] INFO Kafka version: 2.7.0 (org.apache.kafka.common.utils.AppInfoParser)
Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,553] INFO Kafka commitId: 448719dc99a19793 (org.apache.kafka.common.utils.AppInfoParser)
Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,553] INFO Kafka startTimeMs: 1613406099533 (org.apache.kafka.common.utils.AppInfoParser)
Feb 15 16:21:39 debian kafka-server-start.sh[6006]: [2021-02-15 16:21:39,554] INFO [KafkaServer id=0] started (kafka.server.KafkaServer)
```

### stop

```shell
# systemctl stop kafka.service
```