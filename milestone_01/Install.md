# Installation

## Java JRE

```
apt-get install default-jre
```

## Zookeeper

```shell
mkdir -p /opt/zookeeper
```


```shell
/sbin/groupadd zookeeper
/sbin/useradd -s /bin/bash -M -d /opt/zookeeper -g zookeeper zookeeper
```

Download ZooKeeper binaries from [releases web page](https://zookeeper.apache.org/releases.html)

```
cd /opt/zookeeper
wget https://downloads.apache.org/zookeeper/zookeeper-3.6.2/apache-zookeeper-3.6.2-bin.tar.gz
```

Extract and change folder ownership

```
tar -xf apache-zookeeper-3.6.2-bin.tar.gz
chown -R zookeeper:zookeeper apache-zookeeper-3.6.2-bin
ln -s apache-zookeeper-3.6.2-bin 3.6.2
```

```shell
ls -l /opt/zookeeper/

lrwxrwxrwx 1 zookeeper zookeeper   26 Feb 14 22:38 3.6.2 -> apache-zookeeper-3.6.2-bin
drwxr-xr-x 6 zookeeper zookeeper 4096 Feb 14 22:38 apache-zookeeper-3.6.2-bin
```


## Kafka

```shell
mkdir -p /opt/kafka
```

```shell
/sbin/groupadd kafka
/sbin/useradd -s /bin/bash -M -d /opt/kafka/ -g kafka kafka
```

Download kafka binaries from [releases web page](https://kafka.apache.org/downloads)

```
cd /opt/kafka
wget https://downloads.apache.org/kafka/2.7.0/kafka_2.13-2.7.0.tgz
```

Extract and change folder ownership

```
tar -xf kafka_2.13-2.7.0.tgz
chown -R zookeeper:zookeeper kafka_2.13-2.7.0
ln -s kafka_2.13-2.7.0 2.7.0
```

```shell
ls -l /opt/kafka/
lrwxrwxrwx 1 kafka kafka       16 Feb 14 22:38 2.7.0 -> kafka_2.13-2.7.0
drwxr-xr-x 6 kafka kafka     4096 Dec 16 14:03 kafka_2.13-2.7.0
```
