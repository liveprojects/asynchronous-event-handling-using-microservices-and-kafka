#!/bin/bash


systemctl start zookeeper.service

if [ $? -ne 0 ]
then
    echo "zookeeper failed to start"
    systemctl status zookeeper.service

    exit 1
fi

systemctl start kafka.service

if [ $? -ne 0 ]
then
    echo "kafka failed to start"
    systemctl status kafka.service

    exit 1
fi

exit 0
