#!/bin/bash


systemctl start zookeeper.service

if [ $? -ne 0 ]
then
    echo "zookeeper failed to stop"
    systemctl status zookeeper.service
fi

systemctl stop kafka.service

if [ $? -ne 0 ]
then
    echo "kafka failed to stop"
    systemctl status kafka.service
fi

exit 0