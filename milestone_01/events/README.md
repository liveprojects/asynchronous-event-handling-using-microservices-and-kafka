# Events description

## Failure

Define an event schema representing when a consumer is unable to successfully process and event it has received. This event should contain the event that could not be processed.

| `field` | `description` |
|---|---|
| id | order ID |
| time | timestamp when the error occurred | 
| error_code | error identifier |
| error | error description ( if available ) |

## Notification

Define an event schema representing when an email notification needs to be sent out.

| `field` | `description` |
|---|---|
| recipierd | address the mail has to be sent to |
| object | mail topic | 
| message | mail body |

## Order Received

Define an event schema representing when an order has been received.

| `field`       | `type`    | `description` |
|---------------|-----------|-----| 
| id            | int       | order ID |
| time          | int       | timestamp when the order occurred | 
| prod_id       | int       | ordered product ID |
| prod_desc     | string    | ordered product description |
| customer_id   | int       | customer ID |


## Order Confirmed

Define an event schema representing when an order has been confirmed (is not a duplicate order and can be processed).

| `field`       | `type`    | `description` |
|---------------|-----------|-----| 
| id            | int       | order ID |
| time          | int       | timestamp when the order occurred | 
| prod_id       | int       | ordered product ID |
| prod_desc     | string    | ordered product description |
| customer_id   | int       | customer ID |
| confirmed     | bool      | default `false` |

## Order Ready

Define an event schema representing when an order has been picked from within a warehouse and packed (ready to be shipped).

| `field`       | `type`    | `description` |
|---------------|-----------|-----| 
| id            | int       | order ID |
| time          | int       | timestamp when the order occurred | 
| prod_id       | int       | ordered product ID |
| prod_desc     | string    | ordered product description |
| customer_id   | int       | customer ID |
| confirmed     | bool      | default `true` |
| ready         | bool      | default `false` |