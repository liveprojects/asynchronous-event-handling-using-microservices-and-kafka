
# Zookeeper configuration

As `root` user :

```shell
mkdir -p /data/zookeeper/dataDir
chown -R zookeeper:zookeeper /data/zookeeper
```

Rename and edit file `/opt/zookeeper/3.6.2/conf/zoo_sample.cfg` and change `dataDir` parameter

```shell
mv /opt/zookeeper/3.6.2/conf/zoo_sample.cfg /opt/zookeeper/3.6.2/conf/zoo.cfg
```

```
# the directory where the snapshot is stored.
# do not use /tmp for storage, /tmp here is just 
# example sakes.
dataDir=/data/zookeeper/dataDir
...
```


## Test 

As `zookeeper` user:

```shell
/opt/zookeeper/3.6.2/bin/zkServer.sh --config /opt/zookeeper/3.6.2/conf start
```

```shell
$ netstat -natup | grep 2181
tcp6       0      0 :::2181                 :::*                    LISTEN      4837/java
```

ZooKeeper started correctly