package events

import "time"

type OrderReceived struct {
	ID         uint64    `json:"id"`
	Time       time.Time `json:"time"`
	ProducID   uint32    `json:"prod_id"`
	ProdDesc   string    `json:"prod_desc"`
	CustomerID uint64    `json:"customer_id"`
}
