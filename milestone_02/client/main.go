package main

import (
	"bytes"
	"client/internal/events"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func generateOrder() events.OrderReceived {

	order := events.OrderReceived{
		ID:         1,
		Time:       time.Now(),
		ProducID:   5,
		ProdDesc:   "bla",
		CustomerID: 300,
	}

	return order

}

func registerOrder(order events.OrderReceived, broker string) {

	content, err := json.Marshal(order)

	if err != nil {
		fmt.Println(err.Error())
	}

	if req, err := http.NewRequestWithContext(context.Background(),
		"POST",
		fmt.Sprintf("http://%s/neworder", broker),
		bytes.NewBuffer(content)); err != nil {
		fmt.Println(err.Error())
		return
	} else {

		http.DefaultClient.Do(req)
	}
}

var server string

func init() {

	flag.String("kafka-broker", "127.0.0.1:6000", "kafka broker")

	viper.SetEnvPrefix("CLI")  // All environment variables that match this prefix will be loaded and can be referenced in the code
	viper.AllowEmptyEnv(false) // Allows an environment variable to not exist and not blow up, I suggest using switch statements to handle these though
	viper.AutomaticEnv()       // Do the darn thing :D
	flag.Parse()
	viper.BindPFlags(flag.CommandLine)

}

func main() {

	order := generateOrder()

	registerOrder(order, viper.GetString("kafka-broker"))

}
