## Milestone 2

Content:

* [clinet](./client) : `golang` application 

* [rest_publisher](./rest_publisher) : RestAPI


```mermaid
sequenceDiagram
    Client->>RestAPI: /neworder
    RestAPI-->>KafkaBroker: topic received_orders
```

