package main

import (
	"net/http"
	"rest_publisher/cmd"
)

func main() {

	server := http.Server{
		Addr: ":6000",
	}

	http.HandleFunc("/neworder", cmd.Publish)

	server.ListenAndServe()
}
