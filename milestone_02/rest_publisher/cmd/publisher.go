package cmd

import (
	"fmt"
	"net/http"
	"os"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

/* Publish the POST
 */
func Publish(w http.ResponseWriter, r *http.Request) {

	fmt.Println("new request")

	reqLen := r.ContentLength

	fmt.Println("Content len ", reqLen)

	body := make([]byte, reqLen)

	r.Body.Read(body)

	hn, _ := os.Hostname()

	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": "192.168.2.30:9092",
		"client.id":         hn,
		"acks":              "all"})

	if err != nil {
		fmt.Println(fmt.Sprintf("Failed to create producer: %s\n", err))
		w.WriteHeader(http.StatusInternalServerError)
	}

	topic := "received_orders"

	deliveryChan := make(chan kafka.Event, 10000)
	err = p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          []byte(body)},
		deliveryChan,
	)

	e := <-deliveryChan
	m := e.(*kafka.Message)

	if m.TopicPartition.Error != nil {
		fmt.Printf("Delivery failed: %v\n", m.TopicPartition.Error)
	} else {
		fmt.Printf("Delivered message to topic %s [%d] at offset %v\n",
			*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset)
	}

	close(deliveryChan)

}
