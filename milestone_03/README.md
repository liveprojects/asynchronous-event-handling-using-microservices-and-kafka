# Milestone 3

## Tasks

* Create a new Order microservice in Go.
  * Create an HTTP POST endpoint responsible for receiving a payload that represents an order. 
  * Create a function that will verify that the order payload is valid.
  * Create a function that will translate the order payload into the relevant event schema.
  * Publish an event to the OrderReceived topic in Kafka indicating that an order has been received.
  * Compose the above functions to create an end-to-end solution.

* Test that the service works as expected by posting an order payload to a running service. Verify that the correct message was received by the OrderReceived topic in Kafka.


## Build

```shell
go build [-o order]
```

## Start

```shell
./order -h
Usage of ./order:
      --broker string    Kafka broker endpoint (default "127.0.0.1:9092")
      --service string   GO microservice endpoint (default "127.0.0.1:9090")
      --topic string     Kafka topic where the orders will be published (default "received_orders")
      
./order --broker 192.168.2.30:9092
INFO[0000] Server is starting listening on 127.0.0.1:9090
```

```shell
./bin/kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic received_orders
```

```shell
curl -X POST -H 'content-type: application/json' -d '{"id":1, "prod_id":2, "prod_desc":"my favorite product", "customer_id":1200}' 127.0.0.1:9090/order -vv
Note: Unnecessary use of -X or --request, POST is already inferred.
*   Trying 127.0.0.1:9090...
* TCP_NODELAY set
* Connected to 127.0.0.1 (127.0.0.1) port 9090 (#0)
> POST /order HTTP/1.1
> Host: 127.0.0.1:9090
> User-Agent: curl/7.68.0
> Accept: */*
> content-type: application/json
> Content-Length: 60
> 
* upload completely sent off: 60 out of 60 bytes
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Tue, 09 Mar 2021 17:34:42 GMT
< Content-Length: 0
< 
* Connection #0 to host 127.0.0.1 left intact

```

```shell
$ ./order --broker 192.168.2.30:9092
INFO[0000] Server is starting listening on 127.0.0.1:9090 
Delivered message to topic received_orders [0] at offset 1
```

```shell
./bin/kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic received_orders

{"id":1,"prod_id":2,"prod_desc":"my favorite product","customer_id":1200,"time":"2021-03-09T17:58:05.9190232+01:00"}

```

## Tasks

* *Create an HTTP POST endpoint responsible for receiving a payload that represents an order.*