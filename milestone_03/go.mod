module order

go 1.15

require (
	github.com/confluentinc/confluent-kafka-go v1.5.2
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.7.1
)
