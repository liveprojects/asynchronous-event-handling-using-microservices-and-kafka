
package events

import (
	"encoding/json"
	"time"
)

type OrderReceived struct {
	ID         uint64    `json:"id"`
	ProductID  uint32    `json:"prod_id"`
	ProdDesc   string    `json:"prod_desc"`
	CustomerID uint64    `json:"customer_id"`
	Time       time.Time `json:"time"`
}


func NewOrder(msg []byte) (*OrderReceived, error) {

	var order OrderReceived

	err := json.Unmarshal(msg, &order)

	if err != nil {
		return nil, err
	}

	order.Time = time.Now()
	
	return &order, nil
}