package cmd

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"order/internal/events"
)

func HanldeRequest(w http.ResponseWriter, r *http.Request) {

	var order *events.OrderReceived
	var err error

	body := make([]byte, r.ContentLength)

	r.Body.Read(body)

	if order, err = events.NewOrder(body); err!= nil {
		log.Error(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err = PublishOrder(*order); err != nil {
		log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)

	return

}


