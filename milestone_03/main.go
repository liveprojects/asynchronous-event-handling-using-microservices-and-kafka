package main

import (
	log "github.com/sirupsen/logrus"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
	"net/http"
	"order/cmd"
)

func start(addr string) {

	server := http.Server{
		Addr:              addr,
		Handler:           nil,
		TLSConfig:         nil,
		ReadTimeout:       0,
		ReadHeaderTimeout: 0,
		WriteTimeout:      0,
		IdleTimeout:       0,
		MaxHeaderBytes:    0,
		TLSNextProto:      nil,
		ConnState:         nil,
		ErrorLog:          nil,
		BaseContext:       nil,
		ConnContext:       nil,
	}

	http.HandleFunc("/order", cmd.HanldeRequest)

	log.Info("Server is starting listening on " + addr)

	log.Error(server.ListenAndServe())

}

func main() {

	flag.String("broker", "127.0.0.1:9092", "Kafka broker endpoint")
	flag.String("topic", "received_orders", "Kafka topic where the orders will be published")
	flag.String("service", "127.0.0.1:9090", "GO microservice endpoint")

	viper.BindPFlags(flag.CommandLine)

	flag.Parse()

	start(viper.GetString("service"))
}
